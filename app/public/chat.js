/*################################################################################################*/

// Establish Connection
const socket = io.connect('http://localhost:4000');

/*################################################################################################*/

// Query Dom
const message   = document.getElementById('message'),
      handle    = document.getElementById('handle'),
      btn       = document.getElementById('send'),
      output    = document.getElementById('output'),
      feedback  = document.getElementById('feedback');

/*################################################################################################*/

// Emit Events to Server

/*
  https://socket.io/docs/server-api/#namespace-emit-eventName-%E2%80%A6args
    namespace.emit(eventName[, …args])
*/
btn.addEventListener('click', () => { 
  socket.emit('chat', { 
    message: message.value, handle: handle.value
  });
  message.value = ''; // Clear or reset the value of message
})

message.addEventListener('keypress', () => { socket.emit('typing', handle.value) })


/*################################################################################################*/

// Listen for events from Server

socket.on('chat', (data) => {
  feedback.innerHTML = '';
  output.innerHTML  += ` <p><strong>' ${data.handle} : </strong>' ${data.message}'</p> `;
})

socket.on('typing', (data) => { 
  feedback.innerHTML = `<p><em>' ${data}' is typing a message...</em></p>`;
})

/*################################################################################################*/
