const express = require('express');
const socket = require('socket.io');

// App Setup
const app = express();
const server = app.listen(4000, () => { console.log('listening on 4000')})

// Static Directory
app.use(express.static('./public'));

// Socket Setup and pass server
/*
  https://socket.io/docs/client-api/#io-protocol
  With query parameters
*/
const io = socket(server);
io.on('connection', (socket) => {
  // Handle Chat Event
  socket.on('chat', (data) => { io.sockets.emit('chat', data) });

  // Broadcast the typing event to all clients except for the creator.
  socket.on('typing', (data) => { socket.broadcast.emit('typing', data) })
})